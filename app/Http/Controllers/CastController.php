<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function cast(Request $request){
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = blog::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function index(){
        $cast = blog::table('cast')->get();
        return view('cast.index', compact('casts'));
    }

    public function show($id){
        $cast = blog::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = blog::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = blog::table('cast')
        ->where('id', $id)
        ->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function destroy($id){
        $query = blog::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
